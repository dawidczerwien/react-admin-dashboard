import React from 'react';
import { NotificationsNone, Language, Settings } from '@material-ui/icons';
import './topbar.css';
const Topbar = () => {
  return (
    <div className='topbar'>
      <div className='topbarWrapper'>
        <div className='topLeft'>
          <span className='logo'>Admin</span>
        </div>
        <div className='topRight'>
          <div className='topbarIconContainer'>
            <NotificationsNone />
            <span className='topIconBag'>2</span>
          </div>
          <div className='topbarIconContainer'>
            <Language />
          </div>
          <div className='topbarIconContainer'>
            <Settings />
          </div>
          <img
            src='https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png'
            alt='avatar'
            className='topAvatar'
          />
        </div>
      </div>
    </div>
  );
};

export default Topbar;
